<?php
// intialisation de la variable $msg concernant l'information à indiquer à l'utilisateur
$msg = "";

// si de la donnée envoyée via "POST" est récupérée alors on vérifie la date
if (isset($_POST['jour'])) {
	// appel à la fonction de vérification
	$msg = verifierInput("jour");
}

/**
 * Prend une date en paramètre et vérifie son format
 * Format attendu : "jj/mm/aaaa"
 * 
 * @param $nomParametrePost Nom du paramètre "POST" à récupérer de la variable globale "$_POST" (autrement dit, le nom de l'input)
 * @return le code HTML qui sera afficher pour l'utilisateur
 */
function verifierInput(string $nomParametrePost)
{
	// initialisation d'une variable de résultat
	$returnHtml = '';

	// TODO : récupérer la date à partir de la variable $_POST : https://www.php.net/manual/en/reserved.variables.post.php
	$dateSaisie = ????????;

	// construction du début du message qui permettra d'indiquer à l'utilisateur la date saisie
	$returnHtml .= "<span style='color: #48A2F5'>Date saisie : $dateSaisie </span><br>";

	// TODO : construire une expression régulière pour vérifier le format de la date "jj/mm/aaaa" et uniquement le format 
	// pour apprendre à concevoir une expression régulière référez vous au tutoriel suivant : https://regexlearn.com/fr/learn/regex101
	// faites les exercices de 1 à 32, ceci vous permettra de trouver une solution
	// pour tester votre regex vous pourrez utiliser le site : https://regex101.com/
	$regex = ????????;

	// utilisation de la regex avec preg_match
	$matchOk = preg_match($regex, $dateSaisie);

	// TODO : compléter le test du "if"
	if (???????) {
		// le format de la date n'est pas correct
		$returnHtml .= "<div style='color: red'>Le format de la date ne correspond pas au masque attendu !!!</div>";
	} else {
		// le format de la date est correct
		$returnHtml .= "<div style='color: green'>Le format de la date est valide !!!</div>";

		// reste à vérifier si la date est réaliste
		// TODO : récupérer 3 variable $mois, $jour et $année qui seront véirifier avec "checkdate

		// La fonction checkdate() de PHP vérifie si le mois, le jour et l'année sont réalistes
		if (checkdate($mois, $jour, $annee)) {
			$returnHtml .= "<span style='color: green'>La date est ok ! </span>";
		} else {
			// TODO : ajout d'un message sur le modèle du message de succès
		}
	}
	return $returnHtml;
}

?>

<!DOCTYPE html>
<html>

<head>
	<link href="css/bootstrap.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/style.css" />
	<link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great" rel="stylesheet">

	<title>Validation d'une date</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<div class='MonTableau'>
		<h2>Validation Date</h2>
		<form method="post" action="<?php echo $_SERVER["SCRIPT_NAME"] ?>">
			Entrez une date : <input type="text" name="jour" placeholder="jj/mm/aaaa">
			<input class="btn btn-sample" type="submit" name="b" value="Tester">
			<!-- Affichage du message composé par la fonction "vérifier" -->
			<div><?php echo $msg; ?></div>
		</form>
	</div>
</body>

</html>