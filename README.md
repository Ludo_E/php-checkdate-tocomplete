# Validation du format d'une date

## Objectif

Tâche au choix :
- utilisez et complétez le code fourni
- ré-implémentez le code "from scratch"

La fonctionnalité attendue est présentée par le gif suivant :
![Gif de présentation de la fonctionnalité](./objectif.gif)

### Implémentation à partir du code fourni

1. Analysez le code pour comprendre son fonctionnement
2. Complétez les "TODO

### Implémentation "from scratch"

1. Créez une page PHP contenant un formulaire, un champ de type texte, et un bouton pour soumettre la requête. Le formulaire devra être en "POST".
2. Définissez une fonction PHP de vérification de date : `function verifierInput(string $nomParametrePost)` qui prendre en paramètre le nom d'un paramètre que vous pourrez retrouver dans la variable ["$_POST"](https://www.php.net/manual/fr/reserved.variables.post.php).
3. Commencez par afficher la valeur du champ correspondant : un simple echo suffira.
4. Concevez une expression régulière pour vérifier le format **"jj/mm/aaaa"**.
5. Modifiez la fonction `verifierInput` pour qu’elle renvoie un message d'erreur lorsque le champ n'est pas validé par la regex.
6. Ajoutez à votre code une vérification de la date, en fait il faut contrôler cette fois si elle correspond bien à une date réaliste. Pour se faire vous pourrez utiliser la fonction PHP "[checkdate(int $month, int $day, int $year): bool](https://www.php.net/manual/en/function.checkdate.php)".

## Tester votre code
Commande utilisée pour lancer un serveur de développement PHP (à exécuter à la racine de votre projet) :
```bash
php -S localhost:8000 -d display_errors=1
```

Puis connectez vous à l'URL
```
localhost:8000/checkdate.php
```
